# Autor: Petr Urban, urbanp13
#
# spousteni:
# scrapy runspider idnesCrawler.py -o data_idnes.cz.json

import scrapy
import time
from scrapy.conf import settings


class BlogSpider(scrapy.Spider):
    name = 'iDnes.cz crawler'

    allowed_domains = ['idnes.cz']
    settings.overrides['DEPTH_LIMIT'] = 2
    settings.overrides['FEED_EXPORT_ENCODING'] = 'utf-8'

    # delay neni nutne nastavovat (viz idnes.cz/robots.txt)
    # download_delay = 1

    # pro hledani souvisejicich clanku
    #start_urls = ['http://zpravy.idnes.cz/nemecko-razie-ctvrt-moabit-mesita-dlx-/zahranicni.aspx?c=A170228_094303_zahranicni_ale']

    # pro prolezani celeho idnes.cz
    start_urls = ['http://idnes.cz/']



    def parse(self, response):
        if response.css("div.opener") or response.css("div.perex"):

            headline = response.css("h1::text").extract_first().strip()             # nazev clanku

            if response.css("div.opener"):  # perex je vesinou pojmenovan opener, v nekterych rubrikach perex
                perex = response.css("div.opener::text").extract_first().strip()
            else:
                perex = response.css("div.perex::text").extract_first().strip()

            author = response.css("[itemprop='name']::text").extract_first()        # jmeno hlavniho autora
            if author is None:
                author = response.css("div.authors > span::text").extract_first()   # v pripade blogu je jina struktura polozky autor
                if author is not None:
                    author = str(author)[7:]
            if author is None:                                                      # autor nekdy neni vubec uveden
                author = 'Neuveden'

            if headline != "":                                                      # ulozeni do *.json
                yield {'headline': headline,
                       'perex': perex,
                       'author': author
                       }



        links = response.css("h3 > a::attr(href)").extract()                        # ziskani "vsech" odkazu (v pripade uvodni stranky)
        relatedLinks = response.css("div.list-art > div.art > h3 > a::attr(href)").extract() # odkazy na souvisejici clanky (u jednotlivych clanku)


        for link in links:          # zde lze misto links pouzit relatedLinks (pokud zaciname z clanku, zavisle na start_urls)
            yield scrapy.Request(response.urljoin(link), callback=self.parse)
